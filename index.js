const replacer = cache => (key, value) => {
  if (typeof value === 'object' && value !== null) {
    if (cache.indexOf(value) !== -1) {
      try {
        return JSON.parse(JSON.stringify(value));
      } catch (error) {
        return;
      }
    }
    cache.push(value);
  }
  return value;}

function stringify(o) {
  const cache = []

  return JSON.stringify(o, replacer(cache), 2)
}

module.exports = {
  stringify
}

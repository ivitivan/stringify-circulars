const { stringify } = require('./index')
const { equals } = require('ramda')

const assert = (trueish, message) => {
  if (!trueish) throw new Error(message)
}

{
  const circularDependency = {}
  circularDependency.circularDependency = circularDependency

  const actual = stringify(circularDependency)
  const expected = '{}';

  assert(equals(actual, expected), 'should stringify objects with cyclic object value')
}
